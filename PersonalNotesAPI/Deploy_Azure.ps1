﻿param
(   
    [string]$Azure_CredentialFilePath = ".\AzureProfile_CuongDuong.json",
    [string]$Azure_ResourceLocation = "UK South",
    [string]$Azure_ResourceGroupName = "CuongDuongResourceGroupName",
    [string]$Azure_AppServicePlan = "CuongDuongAppServicePlan",
    [string]$Azure_AppServicePlanTier = "Free",
    #Storage account name must be between 3 and 24 characters in length and use numbers and lower-case letters only.
    [string]$Azure_StorageAccountName = "cuongduongsa",
	[string]$Azure_StorageContainerName = "cuongduongcontainer",
    [string]$Azure_RelayNameSpace = "cuongduongrns",
    [string]$Azure_HybridConnectionName = "cuongduonghc",
    [string]$Azure_AppRegistrationName = "cuongduonglbgmiapp",
    [string[]]$Azure_ADGroupNames = @("cddgroup1","cddgroup2"),
    [string]$WebMVC_Name = "cuongduonglbgmi",
    [string]$WebMVC_SlotName = "",
    [string]$WebAPI_ServerName = "UKDD1AP50",
    [string]$WebAPI_Port = "30458"
)


$NewLine = [Environment]::NewLine
Write-Host "$NewLine*********START*********"

Write-Host "$NewLine -> Checking system..."
Write-Host "$NewLine -> Current host is"
Get-Host

Write-Host "$NewLine -> Checking PowerShell..."
$PSVersionTable.PSVersion

if (($PSVersionTable.PSVersion.Major -lt 5) -or (($PSVersionTable.PSVersion.Major -eq 5) -and ($PSVersionTable.PSVersion.Minor -lt 1))){
    throw "ERR! Please intall PowerShell 5.1 first. Read this link https://docs.microsoft.com/en-us/powershell/wmf/5.1/install-configure"
}
else{
    Write-Host "PASSED!"
}

Write-Host "$NewLine -> Checking Web Deploy..."
$WebDeployVersion = get-childitem "HKLM:\SOFTWARE\Microsoft\IIS Extensions\MSDeploy" | Select -last 1 -ErrorVariable notPresent -ErrorAction SilentlyContinue
if ($NotPresent)
{
    throw "ERR! Web Deploy does not exist. Read this link https://www.microsoft.com/en-us/download/details.aspx?id=43717"
}
else
{
    if($WebDeployVersion.PSChildName -lt 3)
    {
        throw "ERR! Need verion 3.6. Read this link https://www.microsoft.com/en-us/download/details.aspx?id=43717"
    }

    $WebDeployFolderPath = (get-childitem "HKLM:\SOFTWARE\Microsoft\IIS Extensions\MSDeploy" | Select -last 1).GetValue("InstallPath")
    $WebDeployPath = "$WebDeployFolderPath" + "msdeploy.exe"
    Write-Host $NewLine"MS deploy exe is $WebDeployPath"
    Write-Host "PASSED!"
}

Write-Host "$NewLine -> Installing required packages..."

#Install required packages
<#Install-PackageProvider -Name NuGet -Force
Install-Module AzureRM -AllowClobber#>


Write-Host "SUCCESS!"


#Login to Azure
Write-Host "$NewLine -> Trying to login Azure..."

#Connect-AzureRmAccount   

Write-Host "Login Azure with json file"
$ResultLogin = Import-AzureRmContext -path $Azure_CredentialFilePath -ErrorAction SilentlyContinue
if(!$ResultLogin){
    Write-Host "ERR! Credential is not valid"
    Write-Host "Trying to login with popup"
    Login-AzAccount -ErrorAction Stop
    Write-Host "SUCCESS!"
    Write-Host "Trying to update credential file"
    Save-AzureRmContext -Path $Azure_CredentialFilePath -ErrorAction SilentlyContinue
    Write-Host "SUCCESS!"
}

Write-Host "SUCCESS!"

#Create Azure resouce group
Write-Host "$NewLine -> Trying to create new resouce group..."
$NotPresent = Get-AzureRmResourceGroup -Name $Azure_ResourceGroupName -ErrorAction SilentlyContinue

if (!$NotPresent)
{
    New-AzureRmResourceGroup -Name $Azure_ResourceGroupName -Location $Azure_ResourceLocation
    Write-Host "SUCCESS!"
}
else
{
    Write-Host "SUCCESS! It is existing."
}

#Create Azure app service plan
Write-Host "$NewLine -> Trying to create new app service plan..."
$NotPresent = Get-AzureRmAppServicePlan -Name $Azure_AppServicePlan -ErrorAction SilentlyContinue
if (!$NotPresent)
{
    New-AzureRmAppServicePlan -Name $Azure_AppServicePlan -Location $Azure_ResourceLocation -ResourceGroupName $Azure_ResourceGroupName -Tier $Azure_AppServicePlanTier
    Write-Host "SUCCESS!"
}
else
{
    Write-Host "SUCCESS! It is existing."
}

#Create Azure storage account
Write-Host "$NewLine -> Trying to create new storage account..."
$NotPresent = Get-AzureRmStorageAccount -ResourceGroupName $Azure_ResourceGroupName -AccountName $Azure_StorageAccountName -ErrorAction SilentlyContinue
if (!$NotPresent)
{
    $StorageAccount = New-AzureRmStorageAccount -ResourceGroupName $Azure_ResourceGroupName -AccountName $Azure_StorageAccountName -Location $Azure_ResourceLocation -SkuName Standard_LRS -Kind Storage -ErrorAction Stop
    Write-Host "SUCCESS!"
}
else
{
    Write-Host "SUCCESS! It is existing."
}

#Get Storage context
Write-Host "$NewLine -> Trying to get current storage context..."
Set-AzureRmCurrentStorageAccount -StorageAccountName $Azure_StorageAccountName -ResourceGroupName $Azure_ResourceGroupName
Write-Host "SUCCESS!"

#Create Azure storage container
Write-Host "$NewLine -> Trying to create new storage container..."
$NotPresent =  Get-AzureStorageContainer -Name $Azure_StorageContainerName -ErrorAction SilentlyContinue
if (!$NotPresent)
{
    New-AzureStorageContainer -Name $Azure_StorageContainerName -ErrorAction Stop
    Write-Host "SUCCESS!"
}
else
{
    Write-Host "SUCCESS! It is existing."
}

#Create Azure Relay namespace
Write-Host "$NewLine -> Trying to create new relay namespace..."
$NotPresent =  Get-AzureRmRelayNamespace -Name $Azure_RelayNameSpace -ResourceGroupName $Azure_ResourceGroupName -ErrorAction SilentlyContinue
if (!$NotPresent)
{
    New-AzureRmRelayNamespace -Name $Azure_RelayNameSpace -ResourceGroupName $Azure_ResourceGroupName -Location $Azure_ResourceLocation -ErrorAction Stop
    Write-Host "SUCCESS!"
}
else
{
    Write-Host "SUCCESS! It is existing."
}

#Create Azure Hybrid connection
Write-Host "$NewLine -> Trying to create new hybrid connection..."
$NotPresent =  Get-AzureRmRelayHybridConnection -Name $Azure_HybridConnectionName -ResourceGroupName $Azure_ResourceGroupName -Namespace $Azure_RelayNameSpace -ErrorAction SilentlyContinue
if (!$NotPresent)
{
    New-AzureRmRelayHybridConnection -Name $Azure_HybridConnectionName -ResourceGroupName $Azure_ResourceGroupName -Namespace $Azure_RelayNameSpace -RequiresClientAuthorization $True -ErrorAction Stop
    Write-Host "SUCCESS!"
}
else
{
    Write-Host "SUCCESS! It is existing."
}

#Create Azure App Registration
Write-Host "$NewLine -> Trying to create new app registration..."
$NotPresent = Get-AzureRmADApplication -DisplayName $Azure_AppRegistrationName -ErrorAction SilentlyContinue
if (!$NotPresent)
{
    $WebMVC_URL = "https://" + $WebMVC_Name + ".azurewebsites.net"
    $WebMVC_ReplyURL = "https://" + $WebMVC_Name + ".azurewebsites.net/.auth/login/aad/callback"
    New-AzureRmADApplication -DisplayName $Azure_AppRegistrationName -HomePage $WebMVC_URL -IdentifierUris $WebMVC_URL -ReplyUrls $WebMVC_ReplyURL -ErrorAction Stop
    Write-Host "SUCCESS!"
}
else
{
    Write-Host "SUCCESS! It is existing."
}

#Create Azure AD Groups
Write-Host "$NewLine -> Trying to create new AD groups..."
Foreach($GroupName in $Azure_ADGroupNames){
    $NotPresent = Get-AzureRmADApplication -DisplayName $Azure_AppRegistrationName -ErrorAction SilentlyContinue
    if (!$NotPresent)
    {
        $WebMVC_URL = "https://" + $WebMVC_Name + ".azurewebsites.net"
        $WebMVC_ReplyURL = "https://" + $WebMVC_Name + ".azurewebsites.net/.auth/login/aad/callback"
        New-AzureRmADApplication -DisplayName $Azure_AppRegistrationName -HomePage $WebMVC_URL -IdentifierUris $WebMVC_URL -ReplyUrls $WebMVC_ReplyURL -ErrorAction Stop
        Write-Host "SUCCESS!"
    }
    else
    {
        Write-Host "SUCCESS! It is existing."
    }
}



<#
$curdirectoryPath = (Get-Item -Path ".\" -Verbose).FullName
Write-Host "$NewLine -> Current directory path: $curdirectoryPath"

$PackageFolderPath = "$curdirectoryPath/Package"

Write-Host "$NewLine -> Checking package folder..."
if (!(Test-Path $PackageFolderPath)) {
  throw "ERR! Package folder $PackageFolderPath does not exist"
}
else {
    Write-Host "SUCCESS!"
}

Write-Host "$NewLine -> Trying to login Azure..."
Login-AzAccount        
Write-Host "SUCCESS!"

$tempFile =".\PublishingProfileTemp.xml"
Write-Host "$NewLine -> Getting publishing profile for $WebMVC_Name app"
if([string]::IsNullOrWhiteSpace($WebMVC_SlotName)){
    $xml = Get-AzureRmWebAppPublishingProfile -Name $WebMVC_Name `
           -ResourceGroupName $Azure_ResourceGroupName `
           -OutputFile $tempFile -Format WebDeploy -ErrorAction Stop
}
else{
    $xml = Get-AzureRmWebAppSlotPublishingProfile -Name $WebMVC_Name `
           -ResourceGroupName $Azure_ResourceGroupName `
           -Slot $WebMVC_SlotName `
           -OutputFile $tempFile -Format WebDeploy -ErrorAction Stop
}
Write-Host "SUCCESS!"

$username = ([xml]$xml).SelectNodes("//publishProfile[@publishMethod=`"MSDeploy`"]/@userName").value
$password = ([xml]$xml).SelectNodes("//publishProfile[@publishMethod=`"MSDeploy`"]/@userPWD").value
$url = ([xml]$xml).SelectNodes("//publishProfile[@publishMethod=`"MSDeploy`"]/@publishUrl").value
$siteName = ([xml]$xml).SelectNodes("//publishProfile[@publishMethod=`"MSDeploy`"]/@msdeploySite").value
if (Test-Path $tempFile) {
    del $tempFile
}

Write-Host "$NewLine -> Getting publishing profile XML..."
if([string]::IsNullOrWhiteSpace($WebMVC_SlotName)){
    Stop-AzureRmWebApp  -Name $WebMVC_Name -ResourceGroupName $Azure_ResourceGroupName
}
else{
    Stop-AzureRmWebAppSlot  -Name $WebMVC_Name -ResourceGroupName $Azure_ResourceGroupName -Slot $WebMVC_SlotName
}
Write-Host "SUCCESS!"


#Backup Website
$StorageAccountData = Get-AzureRmStorageAccountKey -ResourceGroupName $Azure_ResourceGroupName -AccountName $Azure_StorageAccountName

if($StorageAccountData -And $StorageAccountData.Count -gt 0)
{
    $StorageAccountKey = $StorageAccountData.GetValue(0).Value
}
else
{
    throw "Don't find key of StorageAccountName: $Azure_StorageAccountName; ResourceGroupName:$Azure_ResourceGroupName"
}
$StorageContext = New-AzureStorageContext -StorageAccountName $Azure_StorageAccountName -StorageAccountKey $StorageAccountKey

$now = Get-Date
$sasUrl = New-AzureStorageContainerSASToken -Name $Azure_StorageContainerName -Permission rwdl -StartTime $now.AddHours(-1) -ExpiryTime $now.AddMonths(1) -Context $StorageContext -FullUri  

Write-Host "$NewLine -> Backuping website..."
$resultNewBackup = New-AzureRmWebAppBackup -ResourceGroupName $Azure_ResourceGroupName -Name $WebMVC_Name  -Slot $WebMVC_SlotName -StorageAccountUrl $sasUrl

#Wait to backup complete
For ($i=0; $i -le 50; $i++) {
    Start-Sleep -s 30
    $WebAppBackupList = Get-AzureRmWebAppBackup -ResourceGroupName $Azure_ResourceGroupName -Name $WebMVC_Name -Slot $WebMVC_SlotName -BackupId $resultNewBackup.BackupId
    if($WebAppBackupList)
    {
        $BackupStatus = $WebAppBackupList.BackupStatus
        if($BackupStatus -like '*Succeeded*')
        {
            break;
        }

    }
    else
    {
        throw "Can't get backup list from ResourceGroupName:$Azure_ResourceGroupName; WebsiteName:$WebMVC_Name"
    }
}
Write-Host "SUCCESS!"

#Deploy Website
Write-Host "$NewLine -> Deploying website..."
$msdeployArguments = 
    '-verb:sync ' +
    "-source:contentPath='$PackageFolderPath' " +     
    "-dest:contentPath='$siteName',ComputerName='https://$url/msdeploy.axd?site=$siteName',UserName=$username,Password=$password,AuthType='Basic' " +
    "-skip:skipaction=Delete,objectname=dirPath,absolutepath=\\Config " +
    "-skip:skipaction=Delete,objectname=dirPath,absolutepath=\\logs " +
	"-skip:skipaction=Delete,objectname=filePath,absolutepath=\\Web.config"

$commandLine = '&"$WebDeployFolderPath\msdeploy.exe" --% ' + $msdeployArguments
Invoke-Expression $commandLine

if([string]::IsNullOrWhiteSpace($WebMVC_SlotName)){
    Start-AzureRmWebApp  -Name $WebMVC_Name -ResourceGroupName $Azure_ResourceGroupName
}
else{
    Start-AzureRmWebAppSlot  -Name $WebMVC_Name -ResourceGroupName $Azure_ResourceGroupName -Slot $WebMVC_SlotName
}
Write-Host "SUCCESS!"

Write-Host "$NewLine*********FINISH*********"#>