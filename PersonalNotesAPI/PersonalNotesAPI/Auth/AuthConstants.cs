﻿namespace PersonalNotesAPI.Auth
{
    public class AuthConstants
    {
        public const string SecretKey = "Personal Notes .Net Core Api Security Key";
        public const string Audience = "Personal Notes API";
        public const string Issuer = "Personal Note API Clients";
    }
}
