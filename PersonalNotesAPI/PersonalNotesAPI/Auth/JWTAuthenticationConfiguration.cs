﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;

namespace PersonalNotesAPI.Auth
{
    public static class JwtAuthenticationConfiguration
    {
        public static IApplicationBuilder UseJwtProvider(
            this IApplicationBuilder builder, TokenProviderOptions parameters)
        {
            return builder.UseMiddleware<TokenProviderMiddleware>(Options.Create(parameters));
        }
    }
}
