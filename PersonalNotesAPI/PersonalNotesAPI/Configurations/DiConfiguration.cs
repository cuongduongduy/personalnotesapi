﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PersonalNotesAPI.Services;
using PersonalNotesDAO;
using PersonalNotesDAO.Models;
using PersonalNotesDAO.Repositories;
using PersonalNotesDAO.Repositories.Interface;
using PersonalNotesDAO.Services;
using PersonalNotesDAO.Services.Interface;
using PersonalNotesDAO.Uow;
using PersonalNotesDAO.Uow.Interface;

namespace PersonalNotesAPI.Configurations
{
    public partial class DiConfiguration
    {
        public static void Register(IServiceCollection services)
        {
            RegisterRepositories(services);
            RegisterUows(services);
            RegisterServices(services);
        }
    }
}
