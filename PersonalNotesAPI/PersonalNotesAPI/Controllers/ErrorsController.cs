﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace PersonalNotesAPI.Controllers
{
    public class ErrorsController : Controller
    {
        [Route("errors/404")]
        public IActionResult Error404(){

            return View("PageNotFound");
        }

        [Route("errors/concurrency")]
        public IActionResult ErrorConcurrency()
        {
            return View("Concurrency");
        }

        [Route("errors/{code:int}")]
        public IActionResult Error(int code)
        {
            // handle different codes or just return the default error view
            return View();
        }
    }
}