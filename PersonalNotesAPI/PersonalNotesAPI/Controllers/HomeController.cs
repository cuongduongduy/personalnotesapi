﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using PersonalNotesAPI.Models;
using PersonalNotesAPI.ViewModels;

namespace PersonalNotesAPI.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {

            var viewModel = new PageViewModel
            {
                Title = "Welcome"
            };
            return View(viewModel);
        }

        public IActionResult About()
        {
            var viewModel = new PageViewModel
            {
                Title = "About"
            };
            return View(viewModel);
        }

        public IActionResult Contact()
        {
            var viewModel = new PageViewModel
            {
                Title = "Contact"
            };
            return View(viewModel);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
