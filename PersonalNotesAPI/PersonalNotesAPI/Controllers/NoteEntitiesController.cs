﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using PersonalNotesAPI.Models;
using PersonalNotesAPI.Constants;
using System.Collections.Generic;
using PersonalNotesDAO.Models.Notes;
using PersonalNotesDAO.Services.Interface;

namespace PersonalNotesAPI.Controllers
{
    [Authorize]
    public class NoteEntitiesController : Controller
    {

        private readonly INoteEntityService _noteService;

        public NoteEntitiesController(INoteEntityService noteService)
        {
            _noteService = noteService;
        }

        [HttpGet]
        public IEnumerable<Index> GetNoteList()
        {
            var result = _noteService.GetNoteList();
            return result;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var newModel = new PageModel
            {
                Header = "Notes",
                SubHeader = "List"
            };
            return View(newModel);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var viewModel = new PageModel<Create>
            {
                Header = "Notes",
                SubHeader = "Create"
            };
            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([FromBody] [Bind("Title,Description,Reminder")] Create data)
        {
            if (ModelState.IsValid)
            {
                _noteService.AddNote(data);
                var redirectUrl = Url.Action("Index", "NoteEntities");
                return Ok(redirectUrl);
            }

            var viewModel = new PageModel<Create>
            {
                Header = "Notes",
                SubHeader = "Create",
                Content = data
            };
            return View(viewModel);
        }


        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var data = _noteService.GetNoteForEdit(id.Value);
            if (data == null)
            {
                return NotFound();
            }

            var viewModel = new PageModel<Edit>
            {
                Header = "Notes",
                SubHeader = "Edit",
                Content = data
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [FromBody] Edit data)
        {
            if (id != data.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var isExisting = _noteService.CheckExisting(id);
                if (!isExisting)
                {
                    return NotFound();
                }
                _noteService.EditNote(data);

                var redirectUrl = Url.Action("Index", "NoteEntities");
                return Ok(redirectUrl);
            }

            var viewModel = new PageModel<Edit>
            {
                Title = CommonConstant.ApplicationName,
                Header = "Notes",
                SubHeader = "Edit",
                Content = data
            };
            return View(viewModel);
        }
    }
}
