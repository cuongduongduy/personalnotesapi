﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using PersonalNotesAPI.Models;
using PersonalNotesDAO;
using PersonalNotesDAO.Entities;
using PersonalNotesDAO.Models.Notes;
using PersonalNotesDAO.Services.Interface;

namespace PersonalNotesAPI.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/notes")]
    public class NotesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INoteEntityService _noteService;

        public NotesController(ApplicationDbContext context, INoteEntityService noteService)
        {
            _context = context;
            _noteService = noteService;
        }

        [HttpGet]
        public async Task<IEnumerable<Full>> GetList()
        {
            var result = _noteService.GetNoteListFullModel();
            return result;
        }

        [HttpGet("{id}")]
        public async Task<Edit> GetNoteById(int? id)
        {
            if (id == null)
            {
                return null;
            }

            var data = _noteService.GetNoteForEdit(id.Value);
            return data;
        }

        [HttpPut("{id}")]
        public async Task<Edit> SaveNote(int id, [FromBody] Edit note)
        {
            if (id != note.Id)
            {
                return null;
            }

            if (ModelState.IsValid)
            {
                var isExisting = _noteService.CheckExisting(id);
                if (!isExisting)
                {
                    return null;
                }

                _noteService.EditNote(note);
                return note;
            }

            return null;
        }

        [HttpPost]
        public async Task<IActionResult> CreateNote([FromBody] Create note)
        {
            if (ModelState.IsValid)
            {
                _noteService.AddNote(note);
            }
            else
            {
                return new BadRequestResult();
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteNote(int id)
        {
            var isExisting = _noteService.CheckExisting(id);
            if (!isExisting)
            {
                return new NotFoundResult();
            }

            _noteService.DeleteNote(id);
            return Ok();
        }
    }
}
