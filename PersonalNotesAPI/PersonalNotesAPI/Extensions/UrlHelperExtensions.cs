using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using PersonalNotesAPI.Controllers;

namespace PersonalNotesAPI.Extensions
{
    public static class UrlHelperExtensions
    {
        public static object ActiveRouting(ViewContext viewContext, string controller, string action,
            string active = "active")
        {
            var currentController = viewContext.RouteData.Values["Controller"].ToString();
            var currentAction = viewContext.RouteData.Values["Action"].ToString();

            if (string.Compare(controller, currentController, StringComparison.CurrentCultureIgnoreCase) == 0
                &&
                string.Compare(action, currentAction, StringComparison.CurrentCultureIgnoreCase) == 0
            )
            {
                return active;
            }

            return string.Empty;
        }
    }
}
