﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using Microsoft.Extensions.Primitives;

namespace PersonalNotesAPI.Helpers
{
    public static class CookieHelper
    {
        public static IDictionary<string, string> ExtractCookiesFromResponse(HttpResponse response)
        {
            IDictionary<string, string> result = new Dictionary<string, string>();
            if (response.Headers.TryGetValue("Set-Cookie", out var values))
            {
                SetCookieHeaderValue.ParseList(values.ToList()).ToList().ForEach(cookie =>
                {
                    result.Add(cookie.Name.ToString(), cookie.Value.ToString());
                });
            }
            return result;
        }
    }
}
