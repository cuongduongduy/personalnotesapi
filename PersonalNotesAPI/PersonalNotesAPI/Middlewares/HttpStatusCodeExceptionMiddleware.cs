﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PersonalNotesAPI.Extensions;

namespace PersonalNotesAPI.Middlewares
{
    public class HttpStatusCodeExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<HttpStatusCodeExceptionMiddleware> _logger;

        public HttpStatusCodeExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = loggerFactory?.CreateLogger<HttpStatusCodeExceptionMiddleware>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            string result;
            const HttpStatusCode code = HttpStatusCode.InternalServerError;

            _logger.LogError(exception, exception.Message);

            var exceptionType = exception.GetType();

            if (exceptionType == typeof(DbUpdateConcurrencyException))
            {
                result = "Concurrency update, refresh page.";
            }
            else
            {
                result = "An error occurred while processing your request.";
            }

            if (context.Request.IsAjaxRequest())
            {
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int) code;
                return context.Response.WriteAsync(result);
            }

            if (exceptionType == typeof(DbUpdateConcurrencyException))
            {
                context.Response.Redirect("/errors/Concurency");
                return context.Response.WriteAsync(result);
            }
            context.Response.Redirect("/errors/500", true);
            return context.Response.WriteAsync(result);
        }

    }
    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class HttpStatusCodeExceptionMiddlewareExtensions
    {
        public static IApplicationBuilder UseHttpStatusCodeExceptionMiddleware(this IApplicationBuilder builder)
        {
            builder.UseStatusCodePagesWithReExecute("/errors/{0}");

            return builder.UseMiddleware<HttpStatusCodeExceptionMiddleware>();
        }
    }
}
