﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace PersonalNotesAPI.Middlewares
{
    public class MapBearerToCookieMiddleware
    {
        private readonly RequestDelegate _next;

        public MapBearerToCookieMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public Task InvokeAsync(HttpContext context)
        {
            context.Request.Cookies.TryGetValue(".AspNetCore.Identity.Application", out var cookie);
            if (string.IsNullOrWhiteSpace(cookie))
            {
                var bearer = context.Request.Headers["SecurityToken"];
                context.Request.Cookies.Append(new KeyValuePair<string, string>(".AspNetCore.Identity.Application",
                    bearer));
            }

            // Call the next delegate/middleware in the pipeline
            return this._next(context);
        }
    }
    public static class RequestCultureMiddlewareExtensions
    {
        public static IApplicationBuilder MapBearerToCookie(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<MapBearerToCookieMiddleware>();
        }
    }
}
