using System.ComponentModel.DataAnnotations;

namespace PersonalNotesAPI.Models.AccountViewModels
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
