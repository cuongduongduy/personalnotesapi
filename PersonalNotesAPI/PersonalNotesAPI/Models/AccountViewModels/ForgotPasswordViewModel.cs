﻿using System.ComponentModel.DataAnnotations;

namespace PersonalNotesAPI.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
