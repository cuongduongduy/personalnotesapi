﻿namespace PersonalNotesAPI.Models
{
    public class LoginModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool IsSignned { get; set; }

        public LoginModel()
        {
            UserName = "Guest";
            IsSignned = false;
            Email = string.Empty;
        }
    }
}
