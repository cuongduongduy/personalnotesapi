﻿using PersonalNotesAPI.Constants;

namespace PersonalNotesAPI.Models
{
    public abstract class PageModelBase
    {
        public string Title { get; set; }
        public string Header { get; set; }
        public string Url { get; set; }
        public string SubHeader { get; set; }
    }

    public class PageModel : PageModelBase
    {
        public PageModel()
        {
            Title = CommonConstant.ApplicationName;
        }
    }

    public class PageModel<T> : PageModelBase
    {
        public T Content { get; set; }
    }
}
