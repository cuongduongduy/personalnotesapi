﻿using System.Threading.Tasks;

namespace PersonalNotesAPI.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
