﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PersonalNotesAPI.Models;
using PersonalNotesDAO.Models;

namespace PersonalNotesAPI.ViewComponents
{
    public class CurrentLoginDetails : ViewComponent
    {

        #region Properties and variables

        public bool IsSignned => HttpContext.User.Identity.IsAuthenticated;


        #endregion

        #region Constructors

        public CurrentLoginDetails()
        {
        }

        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = await GetLoginDetails();
            return View("CurrentLoginDetails", model);
        }

        private Task<LoginModel> GetLoginDetails()
        {

            if (IsSignned)
            {
                return Task.FromResult(new LoginModel
                {
                    UserName = HttpContext.User.Claims.First(x=>x.Type == "name").Value,
                    Email = HttpContext.User.Claims.First(x => x.Type == "preferred_username").Value,
                    IsSignned = IsSignned
                });
            }
            return Task.FromResult(new LoginModel());
        }


    }
}
