﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PersonalNotesAPI.Models;
using PersonalNotesDAO.Models;

namespace PersonalNotesAPI.ViewComponents
{
    public class LoginManagementViewComponent : ViewComponent
    {

        #region Properties and variables

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public bool IsSignned => _signInManager.IsSignedIn(HttpContext.User);


        #endregion

        #region Constructors

        public LoginManagementViewComponent(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = await GetLoginDetails();
            return View("CurrentLoginDetails", model);
        }

        private Task<LoginModel> GetLoginDetails()
        {

            if (IsSignned)
            {
                return Task.FromResult(new LoginModel
                {
                    UserName = _userManager.GetUserName(HttpContext.User),
                    Email = _userManager.GetUserName(HttpContext.User),
                    IsSignned = IsSignned
                });
            }
            return Task.FromResult(new LoginModel());
        }


    }
}
