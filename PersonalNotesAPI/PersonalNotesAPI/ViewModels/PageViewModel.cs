﻿namespace PersonalNotesAPI.ViewModels
{
    public class PageViewModel
    {
        public string Title { get; set; }
        public string Url { get; set; }
    }
}
