﻿;

(function () {
    var app = personalNotes;
    var namespace = app.constants || {};
    var formatModule = namespace.formats || {};
    formatModule = function (module) {
        module.KENDOUI = {
            DATETIME: "{0:MM/dd/yyyy hh:mm:ss tt}"
        };


        return module;
    }(formatModule);
    namespace.formats = formatModule;

    var messageModule = namespace.messages || {};
    messageModule = function (module) {

        module.ERROR_CONCURRENCY = "Concurrency! Please refresh page";
        module.ERROR_CREATE = "Created failed";
        module.ERROR_UPDATE = "Updated failed";

        module.OK_CREATE = "Created done";
        module.OK_UPDATE = "Updated done";

        return module;
    }(messageModule);
    namespace.messages = messageModule;

    personalNotes.constants = namespace;

}());

