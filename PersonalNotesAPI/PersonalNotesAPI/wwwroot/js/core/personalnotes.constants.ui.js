﻿(function () {
    var app = personalNotes;
    var namespace = app.ui || {};
    var notificationsModule = namespace.notifications || {};
    notificationsModule = function (module) {
        var notification = $("#notification").kendoNotification({
            position: {
                pinned: true,
                top: 30,
                right: 30
            },
            autoHideAfter: 0,
            stacking: "down",
            templates: [
                {
                    type: "error",
                    template: $("#errorTemplate").html()
                }, {
                    type: "success",
                    template: $("#successTemplate").html()
                }]

        }).data("kendoNotification");

        module.success = function (messageText) {
            notification.show({
                message: messageText
            },
                "success");
        };

        module.error = function(messageObject) {
            messageObject = messageObject || { title: '', message: '' };
            notification.show({
                    title: messageObject.title,
                    message: messageObject.message
                },
                "error");
        };

        return module;
    }(notificationsModule);
    namespace.notifications = notificationsModule;
    personalNotes.ui = namespace;
}());