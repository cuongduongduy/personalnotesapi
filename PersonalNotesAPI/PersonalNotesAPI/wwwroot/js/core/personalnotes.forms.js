﻿;

(function () {
    var app = personalNotes;
    var namespace = app.forms || {};
    var utilsModule = namespace.utils || {};
    utilsModule = function (module) {
        module.getRequestVerificationToken = function () {
            var token = $('input[type=hidden][name=__RequestVerificationToken]', document).val();
            return token;
        }
        return module;
    }(utilsModule);
    namespace.utils = utilsModule;
    personalNotes.utilss = namespace;

}());
