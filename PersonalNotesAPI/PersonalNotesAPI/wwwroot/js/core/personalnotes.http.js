﻿;

(function () {
    var app = personalNotes;
    var namespace = app.http || {};
    var utilsModule = namespace.utils || {};
    utilsModule = function (module) {
        module.ajax = function (ajaxOption) {
            ajaxOption = ajaxOption || {};
            var token = personalNotes.utilss.utils.getRequestVerificationToken();
            var defaultOption = {
                method: "GET",
                contentType: "application/json; charset=utf-8",
                cache: false,
                beforeSend: function (request) {
                    request.setRequestHeader("RequestVerificationToken", token);
                },
            };

            ajaxOption = $.extend({}, defaultOption, ajaxOption);
            var request = $.ajax(ajaxOption);
            return request;
        };
        return module;
    }(utilsModule);

    var routingModule = namespace.routing || {};
    routingModule = function (module) {
        var $environmentVariables = $('#environment-variables');

        module.getRootUrl = function () {
            var result = $environmentVariables.find('#root-url').val();
            return result.length === 0 ? '' : result;
        }

        module.getFullUrl = function (link) {
            var rootUrl = personalNotes.http.routing.getRootUrl();
            link = link.replace('//', '/');
            return rootUrl + link;

        }

        module.goUrl = function (fullUrl) {
            window.location.href = fullUrl;
        }
        return module;
    }(routingModule);
    namespace.routing = routingModule;
    namespace.utils = utilsModule;
    personalNotes.http = namespace;

}());
