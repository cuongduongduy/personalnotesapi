﻿(function () {
    var app = personalNotes;
    var notificationsModule = app.speechToText || {};
    notificationsModule = function(module) {

        var $integratedElement, $targetElement;
        var recognition;
        var recognizing, ignoreOnEnd, startTimestamp;
        var finalTranscript;

        module.startRecognition = function ($element, event) {
            $integratedElement = $element;
            $targetElement = $('#' + $integratedElement.data('speechtotext-for'));
            initializeRecognition();
            startRecognition(event);
        }

        function startRecognition(event) {
            if (recognizing) {
                recognition.stop();
            }
            finalTranscript = '';
            recognition.start();
            ignoreOnEnd = false;
            startTimestamp = event.timeStamp;
        }

        var twoLine = /\n\n/g;
        var oneLine = /\n/g;
        function linebreak(s) {
            return s.replace(twoLine, '<p></p>').replace(oneLine, '<br>');
        }

        var firstChar = /\S/;
        function capitalize(s) {
            return s.replace(firstChar, function (m) { return m.toUpperCase(); });
        }

        function upgradeBrowser() {
            personalNotes.ui.notifications.error({
                title: 'Webkit load failed',
                message:
                    'Web Speech API is not supported by this browser. Upgrade to Chrome version 25 or later'
            });
        }

        function onStartRecognition() {
            $integratedElement.find('i').removeClass('fa-microphone').addClass('fa-microphone-alt');
        }

        function onErrorNoSpeech() {
            $integratedElement.find('i').removeClass('fa-microphone-alt').addClass('fa-microphone');
            personalNotes.ui.notifications.error({
                title: "Webkit load failed",
                message:
                    'No speech was detected. You may need to adjust your microphone settings'
            });
        }

        function onErrorAudioCapture() {
            $integratedElement.find('i').removeClass('fa-microphone-alt').addClass('fa-microphone');
            personalNotes.ui.notifications.error({
                title: "Webkit load failed",
                message:
                    'No microphone was found. Ensure that a microphone is installed and that microphone settings are configured correctly'
            });
        }

        function onErrorInfoBlocked() {
            $integratedElement.find('i').removeClass('fa-microphone-alt').addClass('fa-microphone');
            personalNotes.ui.notifications.error({
                title: "Webkit load failed",
                message:
                    'Permission to use microphone is blocked. To change, go to chrome://settings/contentExceptions#media-stream'
            });
        }

        function onErrorInfoDenied() {
            $integratedElement.find('i').removeClass('fa-microphone-alt').addClass('fa-microphone');
                personalNotes.ui.notifications.error({
                    title: "Webkit load failed",
                    message:
                        'Permission to use microphone was denied'
                });
        }

        function onEnd() {
            $integratedElement.find('i').removeClass('fa-microphone-alt').addClass('fa-microphone');
        }

        function initializeRecognition(){
            if (!('webkitSpeechRecognition' in window)) {
                upgradeBrowser();
            } else {
            if (typeof recognition !== 'undefined') {
                return;
            }
                recognition = new window.webkitSpeechRecognition();
                recognition.continuous = true;
                recognition.interimResults = true;
                recognition.lang = 'vi-VN';

                recognition.onstart = function () {
                    recognizing = true;
                    onStartRecognition();
                };

                recognition.onerror = function (event) {
                    if (event.error === 'no-speech') {
                        onErrorNoSpeech();
                        ignoreOnEnd = true;
                    }
                    if (event.error === 'audio-capture') {
                        onErrorAudioCapture();
                        ignoreOnEnd = true;
                    }
                    if (event.error === 'not-allowed') {
                        if (event.timeStamp - startTimestamp < 100) {
                            onErrorInfoBlocked();
                        } else {
                            onErrorInfoDenied();
                        }
                        ignoreOnEnd = true;
                    }
                };

                recognition.onend = function () {
                    recognizing = false;
                    if (ignoreOnEnd) {
                        return;
                    }
                    onEnd();
                };

                recognition.onresult = function (event) {
                    var interimTranscript = '';
                    for (var i = event.resultIndex; i < event.results.length; ++i) {
                        if (event.results[i].isFinal) {
                            finalTranscript += event.results[i][0].transcript;
                        } else {
                            interimTranscript += event.results[i][0].transcript;
                        }
                    }
                    finalTranscript = capitalize(finalTranscript);
                    $targetElement.val(linebreak(finalTranscript) + '' + linebreak(interimTranscript));
                };
            }
        }

        return module;
    }(notificationsModule);
    personalNotes.speechToText = notificationsModule;
}());