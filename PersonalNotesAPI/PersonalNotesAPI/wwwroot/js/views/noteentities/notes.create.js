﻿(function () {
    'use strict';
    var app = personalNotes || {};
    var namespace = app.notes || {};
    var module = namespace.create || {};
    module = (function (module) {

        var $form = $('form#note-create');


        module.initializeElements = function () {
            initializeForm();
            registerEvents();
        };

        function initializeForm() {
            $form.find("#Content_Reminder").kendoDateTimePicker({
                value: new Date(),
                dateInput: true
            });
        }

        function registerEvents() {
            $('button[data-speechtotext-enable="true"]').on('click',
                function() {
                    personalNotes.speechToText.startRecognition($(this), event);
                });
            $form.submit(function () {
                if ($form.valid()) {
                    submitForm();
                }
                return false;
            });
        }

        function submitForm() {
            var data = {
                Title: $form.find('#Content_Title').val(),
                Description: $form.find('#Content_Description').val(),
                Reminder: new Date($form.find('#Content_Reminder').val()).toUTCString()
            };

            var ajaxOptions = {
                method: "POST",
                url: $form.attr('action'),
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data)
            };
            personalNotes.http.utils.ajax(ajaxOptions)
                .done(function (data, textStatus, jqXHR) {
                    var message = personalNotes.constants.messages.OK_CREATE;
                    personalNotes.ui.notifications.success(message);

                    var fullUrl = personalNotes.http.routing.getFullUrl(data);
                    personalNotes.http.routing.goUrl(fullUrl);

                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    var error = {
                        title: personalNotes.constants.messages.ERROR_CREATE,
                        message: jqXHR.responseText
                    }
                    personalNotes.ui.notifications.error(error);
                });
        }

        return module;
    })(module);
    namespace.create = module;
    app.notes = namespace;
}());