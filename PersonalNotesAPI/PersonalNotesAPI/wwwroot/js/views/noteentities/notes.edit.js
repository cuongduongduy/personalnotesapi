﻿(function () {
    'use strict';
    var app = personalNotes || {};
    var namespace = app.notes || {};
    var module = namespace.edit || {};
    module = (function (module) {

        var $form = $('form[id="note-edit"]');


        module.initializeElements = function () {
            initializeForm();
            registerEvents();
        };

        function initializeForm() {
            var value = $form.find("#Content_Reminder").val();
            var dateTime = new Date(value);
            var datetimeValue = dateTime.toLocaleDateString() + ' ' + dateTime.toLocaleTimeString();
            $form.find("#Content_Reminder").kendoDateTimePicker({
                value: datetimeValue,
                dateInput: true
            });
        }

        function registerEvents() {
            $('button[data-speechtotext-enable="true"]').on('click',
                function () {
                    personalNotes.speechToText.startRecognition($(this), event);
                });
            $form.submit(function () {
                if ($form.valid()) {
                    submitForm();
                }
                return false;
            });
        }

        function submitForm() {
            var data = {
                Id: $form.find('#Content_Id').val(),
                Title: $form.find('#Content_Title').val(),
                Description: $form.find('#Content_Description').val(),
                IsDone: $form.find('#Content_IsDone').is(":checked"),
                Reminder: new Date($form.find('#Content_Reminder').val()).toUTCString(),
                Timestamp: $form.find('#Content_Timestamp').val()
            };

            var ajaxOptions = {
                method: "POST",
                url: $form.attr('action'),
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data)
            };
            personalNotes.http.utils.ajax(ajaxOptions)
                .done(function (data, textStatus, jqXHR) {
                    var message = personalNotes.constants.messages.OK_UPDATE;
                    personalNotes.ui.notifications.success(message);
                    var fullUrl = personalNotes.http.routing.getFullUrl(data);
                    personalNotes.http.routing.goUrl(fullUrl);

                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    var error = {
                        title: personalNotes.constants.messages.ERROR_UPDATE,
                        message: jqXHR.responseText
                    }
                    personalNotes.ui.notifications.error(error);
                });
        }

        return module;
    })(module);
    namespace.edit = module;
    app.notes = namespace;
}());