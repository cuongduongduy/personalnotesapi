﻿(function () {
    'use strict';
    var app = personalNotes || {};
    var namespace = app.notes || {};
    var module = namespace.index || {};
    module = (function (module) {

        var $table = $('#note-grid');


        module.initializeElements = function () {
            initializeTable();
        };


        function initializeTable() {
            $table.kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            contentType: "application/json; charset=utf-8",
                            type: "GET",
                            dataType: "json",
                            url: $table.data('source')
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                id: {
                                    type: "number",
                                    editable: false,
                                    required: true
                                },
                                title: { type: "string" },
                                description: { type: "string" },
                                isDone: { type: "boolean"},
                                reminder: { type: "date" }
                            }
                        }
                    },
                    pageSize: 20,
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false
                },
                filterable: true,
                sortable: true,
                pageable: {
                    refresh: true
                },
                columns: [
                    {
                        field: "title",
                        title: "Title"
                    },
                    {
                        field: "description",
                        title: "Description"
                    },
                    {
                        field: "isDone",
                        title: "Is done?",
                        template: "#= isDone ? '<i class=\"fas fa-check green\"></i>' : '' #"
                    },
                    {
                        field: "reminder",
                        title: "Reminder",
                        format: personalNotes.constants.formats.KENDOUI.DATETIME
                    },
                    {
                        field: "",
                        title: "",
                        template: '<a class="k-button k-button-icontext k-grid-edit" href="../NoteEntities/Edit/#=id#"><span class="k-icon k-i-edit"></span>Edit</a>',
                        filterable: false,
                        width: 120
                    }
                ]
            });
        };


        return module;
    })(module);
    namespace.index = module;
    app.notes = namespace;
}());