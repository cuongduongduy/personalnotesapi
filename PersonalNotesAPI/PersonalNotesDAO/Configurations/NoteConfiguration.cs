﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PersonalNotesDAO.Entities;

namespace PersonalNotesDAO.Configurations
{
    internal class NoteConfiguration : IEntityTypeConfiguration<NoteEntity>
    {
        public void Configure(EntityTypeBuilder<NoteEntity> builder)
        {
            builder.ToTable("Notes");
            //builder.Property(x => x.Id).HasColumnName("Id").ValueGeneratedOnAdd();
            builder.Property(x => x.Title).HasColumnName("Title").IsRequired().HasMaxLength(50);
            builder.Property(x => x.Description).HasColumnName("Description").HasMaxLength(255);
            builder.Property(x => x.IsDone).HasColumnName("IsDone").IsRequired();
            builder.Property(x => x.Reminder).HasColumnName("Reminder").IsRequired();

            builder.Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsRequired();
            builder.Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsRequired();
            builder.Property(x => x.UpdatedBy).HasColumnName("UpdatedBy");
            builder.Property(x => x.UpdatedOn).HasColumnName("UpdatedOn");
            builder.Property(x => x.Deleted).HasColumnName("Deleted");


        }
    }
}