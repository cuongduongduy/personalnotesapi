﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PersonalNotesDAO.Entities;

namespace PersonalNotesDAO.Configurations
{
    internal class NoteMappingConfiguration : IEntityTypeConfiguration<NoteEntity>
    {
        public void Configure(EntityTypeBuilder<NoteEntity> builder)
        {
            builder.ToTable("Notes");
            builder.Property(x => x.Title).HasColumnName("Title").IsRequired().HasMaxLength(50);
            builder.Property(x => x.Description).HasColumnName("Description").HasMaxLength(255);
            builder.Property(x => x.IsDone).HasColumnName("IsDone").IsRequired();
            builder.Property(x => x.Reminder).HasColumnName("Reminder").IsRequired();
        }
    }
}