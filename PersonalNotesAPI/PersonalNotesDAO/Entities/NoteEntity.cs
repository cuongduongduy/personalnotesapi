﻿using System;

namespace PersonalNotesDAO.Entities
{
    public class NoteEntity : AuditBaseEntity<int>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsDone { get; set; }
        public DateTime? Reminder { get; set; }
    }
}
