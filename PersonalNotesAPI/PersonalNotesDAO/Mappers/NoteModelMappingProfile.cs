﻿using System;
using AutoMapper;
using PersonalNotesDAO.Entities;
using PersonalNotesDAO.Models.Notes;

namespace PersonalNotesDAO.Mappers
{
    public class NoteModelMappingProfile : Profile
    {
        public NoteModelMappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<NoteEntity, Index>();
            CreateMap<NoteEntity, Full>();

            CreateMap<Create, NoteEntity>()
                .ForMember(d => d.Reminder, opt => opt.MapFrom(x => x.Reminder));

            CreateMap<NoteEntity, Edit>()
                .ForMember(d => d.Reminder, opt => opt.MapFrom(x => x.Reminder ?? DateTime.UtcNow));
            CreateMap<Edit, NoteEntity>()
                .ForMember(d => d.Reminder, opt => opt.MapFrom(x => x.Reminder));

        }
    }
}
