﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PersonalNotesDAO.Models.Notes;

namespace PersonalNotesDAO.Services.Interface
{
    public interface INoteEntityService
    {
        IEnumerable<Index> GetNoteList();
        IEnumerable<Full> GetNoteListFullModel();
        void AddNote(Create note);
        Edit GetNoteForEdit(int id);
        void EditNote(Edit data);
        bool CheckExisting(int id);
        void DeleteNote(int id);
    }
}
