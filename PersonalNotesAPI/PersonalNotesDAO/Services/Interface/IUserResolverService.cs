﻿using System.Collections.Generic;
using System.Security.Claims;

namespace PersonalNotesDAO.Services.Interface
{
    public interface IUserResolverService
    {
        string CurrentUserName();

        IEnumerable<Claim> CurrentUserClaims();
    }
}