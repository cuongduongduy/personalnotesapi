﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using PersonalNotesDAO.Entities;
using PersonalNotesDAO.Models.Notes;
using PersonalNotesDAO.Repositories.Interface;
using PersonalNotesDAO.Services.Interface;
using PersonalNotesDAO.Uow.Interface;

namespace PersonalNotesDAO.Services
{
    public class NoteEntityService : INoteEntityService
    {

        protected readonly IHttpContextAccessor HttpContextAccessor;

        /// <summary>
        /// Private variable. Please prefer to property NoteRepository to call.
        /// </summary>
        private IGenericRepository<NoteEntity, int, ApplicationDbContext> _noteRepository;

        protected IGenericRepository<NoteEntity, int, ApplicationDbContext> NoteRepository =>
            _noteRepository ?? (_noteRepository = HttpContextAccessor.HttpContext.RequestServices
                .GetService<IGenericRepository<NoteEntity, int, ApplicationDbContext>>());

        /// <summary>
        /// Private variable. Please prefer to property Mapper to call.
        /// </summary>
        private IMapper _mapper;

        protected IMapper Mapper => _mapper ?? (_mapper = HttpContextAccessor.HttpContext.RequestServices
                                        .GetService<IMapper>());

        private IUowBase<ApplicationDbContext> _uow;

        protected IUowBase<ApplicationDbContext> Uow => _uow ?? (_uow = HttpContextAccessor.HttpContext.RequestServices
                                                            .GetService<IUowBase<ApplicationDbContext>>());

        public NoteEntityService(IHttpContextAccessor httpContextAccessor)
        {
            this.HttpContextAccessor = httpContextAccessor;
        }

        public IEnumerable<Index> GetNoteList()
        {
            var storedData = NoteRepository.GetAll();
            var result = Mapper.Map<IEnumerable<NoteEntity>, IEnumerable<Index>>(storedData);
            return result;
        }

        public IEnumerable<Full> GetNoteListFullModel()
        {
            var storedData = NoteRepository.GetAll();
            var result = Mapper.Map<IEnumerable<NoteEntity>, IEnumerable<Full>>(storedData);
            return result;
        }

        public void AddNote(Create note)
        {
            var entity = Mapper.Map<Create, NoteEntity>(note);
            NoteRepository.Add(entity);
            Uow.SaveChanges();
        }

        public Edit GetNoteForEdit(int id)
        {
            var storedData = NoteRepository.GetSingle(id);
            var result = Mapper.Map<NoteEntity, Edit>(storedData);
            return result;
        }

        public void EditNote(Edit data)
        {
            var storedData = NoteRepository.GetSingle(data.Id);
            storedData.Title = data.Title;
            storedData.Description = data.Description;
            storedData.IsDone = data.IsDone;
            storedData.Reminder = data.Reminder;
            storedData.Timestamp = data.Timestamp;
            NoteRepository.Edit(storedData);
            Uow.SaveChanges();
        }

        public bool CheckExisting(int id)
        {
            var result = NoteRepository.CheckExistingById(id);
            return result;
        }

        public void DeleteNote(int id)
        {
            var storedData = NoteRepository.GetSingle(id);
            storedData.Deleted = true;
            NoteRepository.Edit(storedData);
            Uow.SaveChanges();
        }
    }
}
