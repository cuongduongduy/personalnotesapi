﻿using Microsoft.EntityFrameworkCore;

namespace PersonalNotesDAO.Uow.Interface
{
    public interface IUowBase<TContext> where TContext : DbContext
    {
        void SaveChanges();
        void SaveChangesAsync();
    }
}
